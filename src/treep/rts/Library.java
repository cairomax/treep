package treep.rts;

public class Library {
	public static Tree height(Tree t) {
		return Tree.create(t.getHeight());
	}

	public static Tree branchingfactor(Tree t) {
		return Tree.create(t.getBranchingFactor());
	}

	public static Tree empty(Tree t) {
		return Tree.create(t.isEmpty());
	}

	public static Tree type(Tree t) {
		return Tree.create(t.getTypeString());
	}
}
