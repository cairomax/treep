package treep.rts;

import java.util.ArrayList;
import java.util.List;

public class Tree {

	private static final Object NULL = new Object();

	private Object data;
	private ArrayList<Tree> children = new ArrayList<>();

	private int height;
	private int branchingFactor;

	private Tree(Object data) {
		if (data instanceof Tree)
			throw new RuntimeException();

		if (data == null)
			data = NULL;
		this.data = data;

		reset();
	}

	public static Tree create(Object root) {
		return new Tree(root);
	}

	public void addChild(Tree child) {
		children.add(child.copy());
		height = Math.max(height, child.height + 1);
		branchingFactor = Math.max(branchingFactor,
				Math.max(children.size(), child.branchingFactor));
	}

	public static Tree eq(Tree a, Tree b) {
		return create(a.data.equals(b.data));
	}

	public static Tree neq(Tree a, Tree b) {
		return create(!a.data.equals(b.data));
	}

	public static Tree plus(Tree a, Tree b) {
		if (a.data instanceof Integer)
			return create((int) a.data + (int) b.data);
		else
			return create((double) a.data + (double) b.data);
	}

	public static Tree minus(Tree a, Tree b) {
		if (a.data instanceof Integer)
			return create((int) a.data - (int) b.data);
		else
			return create((double) a.data - (double) b.data);
	}

	public static Tree times(Tree a, Tree b) {
		if (a.data instanceof Integer)
			return create((int) a.data * (int) b.data);
		else
			return create((double) a.data * (double) b.data);
	}

	public Tree not() {
		return create(!(boolean) data);
	}

	public Tree root() {
		return create(data);
	}

	public boolean isTrue() {
		return (boolean) data;
	}

	public Tree project(int i) {
		return create(((List<?>) data).get(i));
	}

	public void print() {
		System.out.println(data);
	}

	public void assign(Tree other) {
		data = other.data;

		reset();

		for (Tree child : other.children) {
			addChild(child.copy());
		}
	}

	private void reset() {
		children.clear();
		height = 1;
		branchingFactor = 0;
	}

	public static Tree create() {
		return new Tree(null);
	}

	public Object data() {
		return data;
	}

	public static Tree child(Tree tree, Tree index) {
		int i = (int) index.data;

		if (i >= tree.children.size())
			return create();

		return tree.children.get(i);
	}

	public Tree copy() {
		Tree copy = new Tree(data);

		// deep copy
		for (Tree child : children) {
			copy.addChild(child.copy());
		}

		return copy;
	}

	public int getHeight() {
		if (data == NULL)
			return 0;

		return height;
	}

	public int getBranchingFactor() {
		return branchingFactor;
	}

	public boolean isEmpty() {
		return data == NULL;
	}

	public String getTypeString() {
		if (data == NULL)
			return "null";
		if (data instanceof Integer)
			return "int";
		if (data instanceof String)
			return "string";
		if (data instanceof Boolean)
			return "bool";
		if (data instanceof Double)
			return "double";
		throw new AssertionError();
	}
}
