package treep.types;

public enum AtomicType implements Type {
	INT,
	DOUBLE,
	STRING,
	BOOL,

	;

	@Override
	public boolean isNull() {
		return false;
	}

	@Override
	public boolean sameAs(Type other) {
		return this == other;
	}
}
