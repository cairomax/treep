package treep.types;

public interface Type {
	boolean isNull();

	boolean sameAs(Type other);
}
