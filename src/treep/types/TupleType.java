package treep.types;

import java.util.List;

public class TupleType implements Type {
	private List<Type> itemTypes;

	TupleType(List<Type> itemTypes) {
		this.itemTypes = itemTypes;
	}

	public List<Type> getItemTypes() {
		return itemTypes;
	}

	@Override
	public boolean isNull() {
		return false;
	}

	@Override
	public boolean sameAs(Type other) {
		if (other == this)
			return true;

		if (!(other instanceof TupleType))
			return false;

		TupleType otherTuple = (TupleType) other;

		if (itemTypes.size() != otherTuple.itemTypes.size())
			return false;

		for (int i = 0; i < itemTypes.size(); i++)
			if (!itemTypes.get(i).sameAs(otherTuple.itemTypes.get(i)))
				return false;

		return true;
	}
}
