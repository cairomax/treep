package treep.types;

public enum NullType implements Type {
	NULL;

	@Override
	public boolean isNull() {
		return true;
	}

	@Override
	public boolean sameAs(Type other) {
		return other.isNull();
	}
}
