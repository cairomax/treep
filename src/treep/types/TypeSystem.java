package treep.types;

import java.util.List;

import treep.sem.SemanticException;

public class TypeSystem {
	public TupleType tupleType(List<Type> itemTypes) {
		return new TupleType(itemTypes);
	}

	public NullType nullType() {
		return NullType.NULL;
	}

	public Type commonType(Type left, Type right) {
		// TODO: handle tuple types w/ nulls
		return commonAtomicType(left, right);
	}

	private Type commonAtomicType(Type left, Type right) {
		if (!right.isNull()) {
			if (!left.isNull() && !left.sameAs(right))
				throw new SemanticException(String.format(
						"Incompatible types %s and %s.", left, right));
			return right;
		} else {
			return left;
		}
	}

	public boolean assignableFrom(Type type, Type other) {
		return type.isNull() || other.isNull() || other.sameAs(type);
	}
}
