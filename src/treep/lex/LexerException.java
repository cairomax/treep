package treep.lex;

public class LexerException extends RuntimeException {

	public LexerException(String format) {
		super(format);
	}

}
