package treep.lex;

import java.util.regex.Pattern;

public enum Token {
	PAREN_OPEN("\\("),
	PAREN_CLOSE("\\)"),
	TRUE("true"),
	FALSE("false"),
	IF("if"),
	ELSE("else"),
	WHILE("while"),
	RETURN("return"),
	NULL("null"),
	PRINT("print"),
	INT("int"),
	DOUBLE("double"),
	STRING("string"),
	BOOL("bool"),
	DOUBLE_LITERAL("[+-]?[0-9]+\\.[0-9]*"),
	INT_LITERAL("[+-]?[0-9]+"),
	IDE("[a-zA-Z_][a-zA-Z0-9_]*"),
	PLUS("\\+"),
	MINUS("-"),
	TIMES("\\*"),
	EQ("=="),
	NEQ("!="),
	NOT("!"),
	ASSIGN("="),
	BRACE_OPEN("\\{"),
	BRACE_CLOSE("\\}"),
	STRING_LITERAL("\"[^\"]*\""),
	OR("\\|"),
	AND("&"),
	SEMI(";"),
	PROJ_OR_TUPLE_CLOSE(">"),
	ROOT("#"),
	CHILD("@"),
	TREE_OPEN("\\["),
	TREE_CLOSE("\\]"),
	COMMA(","),
	TUPLE_OPEN("<"),

	;

	private Pattern pattern;

	private Token(String regex) {
		pattern = Pattern.compile("^" + regex);
	}

	public Pattern getPattern() {
		return pattern;
	}
}
