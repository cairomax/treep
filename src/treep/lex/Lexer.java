package treep.lex;

import java.util.LinkedList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
	private static final Pattern WHITESPACE = Pattern.compile("^\\s+");
	private Lexicon lexicon = new Lexicon();
	private Scanner scanner;

	private LinkedList<Lexeme> lookaheads = new LinkedList<>();

	private String line = "";

	public Lexer(Readable readable) {
		this.scanner = new Scanner(readable);

		while (hasMoreInput() && lookaheads.size() < 2)
			lookaheads.addLast(scanNext());
	}

	private void advance() {
		if (hasMoreInput())
			lookaheads.addLast(scanNext());

		lookaheads.removeFirst();
	}

	private boolean hasMoreInput() {
		return !line.isEmpty() || scanner.hasNextLine();
	}

	private Lexeme scanNext() {
		ensureToken();

		for (Token token : Token.values()) {
			Pattern pattern = token.getPattern();
			String string = match(pattern);

			if (string == null)
				continue;

			ensureToken();

			return lexicon.lexeme(token, string);
		}

		throw new LexerException(String.format("Unexpected: %s", line));
	}

	private void ensureToken() {
		ensureLine();
		while (match(WHITESPACE) != null)
			ensureLine();
	}

	private void ensureLine() {
		while (line.isEmpty() && scanner.hasNextLine())
			line = scanner.nextLine();
	}

	private String match(Pattern pattern) {
		Matcher matcher = pattern.matcher(line);

		if (!matcher.find())
			return null;

		String string = matcher.group();
		line = line.substring(matcher.end());
		return string;
	}

	public boolean hasNext() {
		return !lookaheads.isEmpty();
	}

	public Lexeme next(Token token) {
		Lexeme next = nextIf(token);
		if (next == null) {
			throw new LexerException(String.format(
					"Expected %s. Found %s instead.", token, lookaheads
							.getFirst().getString()));
		}
		return next;
	}

	public Lexeme nextIf(Token token) {
		Lexeme next = lookaheads.getFirst();
		if (next.getToken() != token)
			return null;
		advance();
		return next;
	}

	public Token lookahead() {
		return lookaheads.getFirst().getToken();
	}

	public Token lookahead2() {
		return lookaheads.get(1).getToken();
	}
}
