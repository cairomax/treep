package treep.lex;

public class Lexeme {
	private Token token;
	private String string;

	Lexeme(Token token, String string) {
		this.token = token;
		this.string = string;
	}

	public Token getToken() {
		return token;
	}

	public String getString() {
		return string;
	}

	@Override
	public String toString() {
		return String.format("%s(%s)", token, string);
	}
}
