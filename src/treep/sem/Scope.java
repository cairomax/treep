package treep.sem;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Scope {
	private Map<String, Variable> definitions = new HashMap<>();
	private Scope parent;

	Scope(Scope parent) {
		this.parent = parent;
	}

	public void add(Variable variable) {
		if (definitions.containsKey(variable.getName()))
			throw new SemanticException(String.format(
					"Variable %s defined multiple times.", variable.getName()));
		definitions.put(variable.getName(), variable);
	}

	public Variable lookup(String name) {
		if (definitions.containsKey(name))
			return definitions.get(name);

		if (parent == null)
			return null;

		return parent.lookup(name);
	}

	public Variable get(String name) {
		Variable variable = lookup(name);

		if (variable == null)
			throw new SemanticException(String.format(
					"Identifier %s not found.", name));

		return variable;
	}

	public Collection<Variable> getAll() {
		return definitions.values();
	}
}
