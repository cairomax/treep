package treep.sem;

public class SemanticException extends RuntimeException {

	public SemanticException(String message) {
		super(message);
	}

}
