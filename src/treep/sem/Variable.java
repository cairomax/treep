package treep.sem;

import treep.gen.Emitter;
import treep.gen.Register;
import treep.gen.RegisterAllocator;
import treep.types.Type;

public class Variable {

	private String name;
	private Type type;

	private Register register;

	Variable(String name, Type type) {
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}

	public void allocateRegisters(RegisterAllocator registerAllocator) {
		register = registerAllocator.nextVar(this);
	}

	public Register getRegister() {
		return register;
	}

	public void emitDeclaration(Emitter emitter) {
		emitter.write(String.format("Tree %s;\n", register.getName()));
	}

}
