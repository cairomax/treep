package treep.sem;

import java.util.List;

import treep.types.Type;

public class Function {

	private String name;
	private Type returnType;
	private List<Type> argumentTypes;

	Function(String name, Type returnType, List<Type> argumentTypes) {
		this.name = name;
		this.returnType = returnType;
		this.argumentTypes = argumentTypes;
	}

	public String getName() {
		return name;
	}

	public Type getReturnType() {
		return returnType;
	}

	public List<Type> getArgumentTypes() {
		return argumentTypes;
	}

}
