package treep.sem;

import java.util.HashMap;
import java.util.Map;

public class Program {
	private Map<String, Function> functions = new HashMap<>();

	public void add(Function function) {
		if (functions.containsKey(function.getName()))
			throw new SemanticException(String.format(
					"Function %s defined multiple times.", function.getName()));
		functions.put(function.getName(), function);
	}

	public Function getFunction(String name) {
		if (!functions.containsKey(name))
			throw new SemanticException(String.format("Undefined function %s.",
					name));
		return functions.get(name);
	}
}
