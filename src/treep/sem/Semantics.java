package treep.sem;

import java.util.List;

import treep.types.Type;

public class Semantics {
	public Variable variable(String name, Type type) {
		return new Variable(name, type);
	}

	public Function function(String name, Type returnType,
			List<Type> argumentTypes) {
		return new Function(name, returnType, argumentTypes);
	}

	public Scope scope(Scope parentScope) {
		return new Scope(parentScope);
	}

	public Program program() {
		return new Program();
	}
}
