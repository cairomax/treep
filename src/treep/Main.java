package treep;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import treep.ast.Parser;
import treep.ast.ProgramDefinition;
import treep.gen.Emitter;
import treep.lex.Lexer;

public class Main {
	public static void main(String[] args) throws IOException {
		ProgramDefinition program = new Parser(new Lexer(new FileReader(
				"example.txt"))).parseProgram();

		program.semanticAnalysis();
		System.out.println(program);

		Emitter emitter = new Emitter(new FileWriter("src/Main.java"));

		program.compile(emitter);

		System.out.flush();
	}
}
