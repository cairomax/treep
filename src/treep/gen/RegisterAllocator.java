package treep.gen;

import treep.sem.Variable;

public class RegisterAllocator {
	private int counter = 0;

	public Register nextExpr() {
		return new Register("expr" + (counter++));
	}

	public Register nextVar(Variable var) {
		return new Register("var_" + var.getName() + "_" + (counter++));
	}

	public Register nextTuple() {
		return new Register("tuple_" + (counter++));
	}
}
