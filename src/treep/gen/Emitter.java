package treep.gen;

import java.io.IOException;
import java.io.Writer;

public class Emitter {
	private Writer writer;

	public Emitter(Writer writer) {
		this.writer = writer;
	}

	public void write(String string) {
		try {
			writer.append(string);
			writer.flush();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
