package treep.ast;

import java.util.ArrayList;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;
import treep.types.Type;
import treep.types.TypeSystem;

public class TreeConstructor extends Expression {

	private Expression root;
	private ArrayList<Expression> children;

	private TypeSystem typeSystem = new TypeSystem();
	private Type type;

	public TreeConstructor(Expression root, ArrayList<Expression> children) {
		this.root = root;
		this.children = children;
	}

	public Expression getRoot() {
		return root;
	}

	public ArrayList<Expression> getChildren() {
		return children;
	}

	@Override
	public void semanticAnalysis() {
		root.setScope(getScope());
		root.setProgram(getProgram());
		root.semanticAnalysis();

		for (Expression expression : children) {
			expression.setScope(getScope());
			expression.setProgram(getProgram());
			expression.semanticAnalysis();
		}

		type = inferType();
	}

	public Type inferType() {
		Type common = root.getType();

		for (Expression child : children) {
			common = typeSystem.commonType(common, child.getType());
		}

		return common;
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(root);
		sb.append("[");

		for (Expression child : children) {
			sb.append(child.toString());
			sb.append(",");
		}

		sb.append("]");

		return sb.toString();
	}

	@Override
	public void onAllocateRegisters(RegisterAllocator registerAllocator) {
		root.allocateRegisters(registerAllocator);
		for (Expression expression : children) {
			expression.allocateRegisters(registerAllocator);
		}
	}

	@Override
	public void compile(Emitter emitter) {
		root.compile(emitter);
		for (Expression child : children) {
			child.compile(emitter);
		}
		emitter.write(String.format("Tree %s = Tree.create(%s.data());\n",
				getOutRegister().getName(), root.getOutRegister().getName()));
		for (Expression child : children) {
			emitter.write(String.format("%s.addChild(%s);\n", getOutRegister()
					.getName(), child.getOutRegister().getName()));
		}
	}
}
