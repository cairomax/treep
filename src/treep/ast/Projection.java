package treep.ast;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;
import treep.sem.SemanticException;
import treep.types.AtomicType;
import treep.types.TupleType;
import treep.types.Type;

public class Projection extends Expression {

	private Expression expression;
	private Literal intLiteral;
	private Type type;

	Projection(Expression expression, Literal intLiteral) {
		this.expression = expression;
		this.intLiteral = intLiteral;
	}

	@Override
	public String toString() {
		return String.format("(%s > %s)", expression.toString(),
				intLiteral.toString());
	}

	@Override
	public void semanticAnalysis() {
		expression.setScope(getScope());
		expression.setProgram(getProgram());

		intLiteral.setScope(getScope());
		intLiteral.setProgram(getProgram());

		expression.semanticAnalysis();
		intLiteral.semanticAnalysis();

		type = inferType();
	}

	public Type inferType() {
		if (!(expression.getType() instanceof TupleType))
			throw new SemanticException(String.format(
					"%s is not a tuple type.", expression.getType()));

		if (intLiteral.getType() != AtomicType.INT)
			throw new AssertionError();

		TupleType tupleType = (TupleType) expression.getType();

		return tupleType.getItemTypes().get(intLiteral.intValue());
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public void onAllocateRegisters(RegisterAllocator registerAllocator) {
		expression.allocateRegisters(registerAllocator);
	}

	@Override
	public void compile(Emitter emitter) {
		expression.compile(emitter);
		emitter.write(String.format("Tree %s = %s.project(%d);\n", getOutRegister()
				.getName(), expression.getOutRegister().getName(), intLiteral
				.intValue()));
	}
}
