package treep.ast;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;

public class Print extends Command {

	private Expression expression;

	Print(Expression expression) {
		this.expression = expression;
	}

	@Override
	public void semanticAnalysis() {
		expression.setScope(getScope());
		expression.setProgram(getProgram());

		expression.semanticAnalysis();
	}

	@Override
	public String toString() {
		return String.format("print %s ;", expression.toString());
	}

	@Override
	public void allocateRegisters(RegisterAllocator registerAllocator) {
		expression.allocateRegisters(registerAllocator);
	}

	@Override
	public void compile(Emitter emitter) {
		expression.compile(emitter);
		emitter.write(String.format("%s.print();\n", expression
				.getOutRegister().getName()));
	}
}
