package treep.ast;

import treep.lex.Lexeme;
import treep.types.AtomicType;

public class BaseTypeSpecifier extends TypeSpecifier {

	private Lexeme lexeme;

	public BaseTypeSpecifier(Lexeme lexeme) {
		this.lexeme = lexeme;
	}

	public Lexeme getLexeme() {
		return lexeme;
	}

	@Override
	public String toString() {
		return String.format("BT(%s)", lexeme.toString());
	}

	@Override
	public AtomicType getType() {
		switch (lexeme.getToken()) {
		case INT:
			return AtomicType.INT;
		case DOUBLE:
			return AtomicType.DOUBLE;
		case STRING:
			return AtomicType.STRING;
		case BOOL:
			return AtomicType.BOOL;
		default:
			throw new AssertionError();
		}
	}
}
