package treep.ast;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;
import treep.lex.Lexeme;
import treep.types.AtomicType;
import treep.types.Type;
import treep.types.TypeSystem;

public class Literal extends Expression {
	private Lexeme lexeme;

	private TypeSystem typeSystem = new TypeSystem();

	private Type type;

	public Literal(Lexeme lexeme) {
		this.lexeme = lexeme;
	}

	public Lexeme getLexeme() {
		return lexeme;
	}

	@Override
	public void semanticAnalysis() {
		type = inferType();
	}

	@Override
	public String toString() {
		return lexeme.getString();
	}

	public Type inferType() {
		switch (lexeme.getToken()) {
		case INT_LITERAL:
			return AtomicType.INT;
		case DOUBLE_LITERAL:
			return AtomicType.DOUBLE;
		case STRING_LITERAL:
			return AtomicType.STRING;
		case TRUE:
		case FALSE:
			return AtomicType.BOOL;
		case NULL:
			return typeSystem.nullType();
		default:
			throw new AssertionError();
		}
	}

	@Override
	public Type getType() {
		return type;
	}

	public int intValue() {
		return Integer.parseInt(lexeme.getString());
	}

	@Override
	public void onAllocateRegisters(RegisterAllocator registerAllocator) {
	}

	@Override
	public void compile(Emitter emitter) {
		emitter.write(String.format("Tree %s = Tree.create(%s);\n",
				getOutRegister().getName(), lexeme.getString()));
	}
}
