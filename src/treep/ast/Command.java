package treep.ast;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;
import treep.sem.Program;
import treep.sem.Scope;

public abstract class Command {

	private Scope scope;
	private Program program;

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public Scope getScope() {
		return scope;
	}

	public void setProgram(Program program) {
		this.program = program;
	}

	public Program getProgram() {
		return program;
	}

	public abstract void semanticAnalysis();

	public abstract void allocateRegisters(RegisterAllocator registerAllocator);

	public abstract void compile(Emitter emitter);
}
