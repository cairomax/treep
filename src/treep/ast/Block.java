package treep.ast;

import java.util.ArrayList;
import java.util.List;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;
import treep.sem.Scope;
import treep.sem.Semantics;
import treep.sem.Variable;

public class Block extends Command {
	private List<Command> commands = new ArrayList<>();

	private Scope innerScope;

	private Semantics semantics = new Semantics();

	public Block(ArrayList<Command> commands) {
		this.commands = commands;
	}

	public List<Command> getCommands() {
		return commands;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("{\n");

		for (Command command : commands) {
			sb.append(command.toString());
			sb.append("\n");
		}

		sb.append("}\n");

		return sb.toString();
	}

	@Override
	public void semanticAnalysis() {
		innerScope = semantics.scope(getScope());

		for (Command command : commands) {
			command.setScope(innerScope);
			command.setProgram(getProgram());
			command.semanticAnalysis();
		}
	}

	public Scope getInnerScope() {
		return innerScope;
	}

	@Override
	public void compile(Emitter emitter) {
		emitter.write(" {\n");

		for (Command command : commands) {
			command.compile(emitter);
		}

		emitter.write("}\n");
	}

	@Override
	public void allocateRegisters(RegisterAllocator registerAllocator) {
		for (Variable variable : innerScope.getAll()) {
			variable.allocateRegisters(registerAllocator);
		}

		for (Command command : commands) {
			command.allocateRegisters(registerAllocator);
		}
	}
}
