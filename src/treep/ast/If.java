package treep.ast;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;

public class If extends Command {
	private Expression guard;
	private Command thenCommand;
	private Command elseCommand;

	public If(Expression guard, Command thenCommand, Command elseCommand) {
		this.guard = guard;
		this.thenCommand = thenCommand;
		this.elseCommand = elseCommand;
	}

	@Override
	public String toString() {
		if (elseCommand == null)
			return String.format("if (%s) %s", guard.toString(),
					thenCommand.toString());
		return String.format("if (%s) %s else %s", guard.toString(),
				thenCommand.toString(), elseCommand.toString());
	}

	@Override
	public void semanticAnalysis() {
		guard.setProgram(getProgram());
		thenCommand.setProgram(getProgram());
		if (elseCommand != null)
			elseCommand.setProgram(getProgram());

		guard.setScope(getScope());
		thenCommand.setScope(getScope());
		if (elseCommand != null)
			elseCommand.setScope(getScope());

		guard.semanticAnalysis();
		thenCommand.semanticAnalysis();
		if (elseCommand != null)
			elseCommand.semanticAnalysis();
	}

	@Override
	public void allocateRegisters(RegisterAllocator registerAllocator) {
		guard.allocateRegisters(registerAllocator);
		thenCommand.allocateRegisters(registerAllocator);
		if (elseCommand != null)
			elseCommand.allocateRegisters(registerAllocator);
	}

	@Override
	public void compile(Emitter emitter) {
		guard.compile(emitter);

		emitter.write(String.format("if ( %s.isTrue() ) {\n", guard
				.getOutRegister().getName()));
		thenCommand.compile(emitter);
		emitter.write("}\n");

		if (elseCommand != null) {
			emitter.write("else {\n");
			elseCommand.compile(emitter);
			emitter.write("}\n");
		}
	}
}
