package treep.ast;

import java.util.ArrayList;
import java.util.List;

import treep.gen.Emitter;
import treep.gen.Register;
import treep.gen.RegisterAllocator;
import treep.types.Type;
import treep.types.TypeSystem;

public class TupleConstructor extends Expression {

	private List<Expression> items;
	private TypeSystem typeSystem = new TypeSystem();
	private Type type;

	private Register tupleRegister;

	public TupleConstructor(List<Expression> items) {
		this.items = items;
	}

	@Override
	public void semanticAnalysis() {
		for (Expression expression : items) {
			expression.setScope(getScope());
			expression.setProgram(getProgram());
			expression.semanticAnalysis();
		}

		type = inferType();
	}

	public Type inferType() {
		ArrayList<Type> itemTypes = new ArrayList<>();

		for (Expression item : items)
			itemTypes.add(item.getType());

		return typeSystem.tupleType(itemTypes);
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public void onAllocateRegisters(RegisterAllocator registerAllocator) {
		for (Expression item : items) {
			item.allocateRegisters(registerAllocator);
		}
		tupleRegister = registerAllocator.nextTuple();
	}

	@Override
	public void compile(Emitter emitter) {
		for (Expression item : items) {
			item.compile(emitter);
		}

		emitter.write(String.format(
				"ArrayList<Object> %s = new ArrayList<Object>();\n",
				tupleRegister.getName()));

		for (Expression item : items) {
			emitter.write(String.format("%s.add(%s.data());\n",
					tupleRegister.getName(), item.getOutRegister().getName()));
		}

		emitter.write(String.format("Tree %s = Tree.create(%s);\n",
				getOutRegister().getName(), tupleRegister.getName()));
	}
}
