package treep.ast;

import treep.types.Type;

public abstract class TypeSpecifier {
	public abstract Type getType();
}
