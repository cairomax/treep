package treep.ast;

import java.util.ArrayList;

import treep.ParsingException;
import treep.lex.Lexeme;
import treep.lex.Lexer;
import treep.lex.Token;

public class Parser {
	private Lexer lexer;
	private Syntax syntax = new Syntax();

	public Parser(Lexer lexer) {
		this.lexer = lexer;
	}

	public ProgramDefinition parseProgram() {
		ArrayList<FunctionDefinition> functions = new ArrayList<>();

		while (lexer.hasNext()) {
			functions.add(parseFunctionDefinition());
		}

		return syntax.program(functions);
	}

	private FunctionDefinition parseFunctionDefinition() {
		TypeSpecifier returnTypeSpecifier = parseTypeSpecifier();

		Identifier identifier = parseIdentifier();

		lexer.next(Token.PAREN_OPEN);

		ArrayList<Argument> arguments = new ArrayList<>();
		if (lexer.lookahead() != Token.PAREN_CLOSE) {
			arguments.add(parseArgument());
		}

		while (lexer.lookahead() != Token.PAREN_CLOSE) {
			lexer.next(Token.COMMA);
			arguments.add(parseArgument());
		}

		lexer.next(Token.PAREN_CLOSE);

		Block block = parseBlock();

		return syntax.functionDefinition(returnTypeSpecifier, identifier,
				arguments, block);
	}

	private Argument parseArgument() {
		TypeSpecifier typeSpecifier = parseTypeSpecifier();
		Identifier identifier = parseIdentifier();

		return syntax.argument(typeSpecifier, identifier);
	}

	private TypeSpecifier parseTypeSpecifier() {
		switch (lexer.lookahead()) {
		case TUPLE_OPEN:
			return parseTupleTypeSpecifier();
		default:
			return parseBaseTypeSpecifier();
		}
	}

	private TupleTypeSpecifier parseTupleTypeSpecifier() {
		lexer.next(Token.TUPLE_OPEN);

		ArrayList<TypeSpecifier> itemTypes = new ArrayList<>();

		if (lexer.lookahead() != Token.PROJ_OR_TUPLE_CLOSE) {
			itemTypes.add(parseTypeSpecifier());
		}

		while (lexer.lookahead() != Token.PROJ_OR_TUPLE_CLOSE) {
			lexer.next(Token.COMMA);
			itemTypes.add(parseTypeSpecifier());
		}

		lexer.next(Token.PROJ_OR_TUPLE_CLOSE);

		return syntax.tupleTypeSpecifier(itemTypes);
	}

	private BaseTypeSpecifier parseBaseTypeSpecifier() {
		switch (lexer.lookahead()) {
		case INT:
			return syntax.baseTypeSpecifier(lexer.next(Token.INT));
		case DOUBLE:
			return syntax.baseTypeSpecifier(lexer.next(Token.DOUBLE));
		case STRING:
			return syntax.baseTypeSpecifier(lexer.next(Token.STRING));
		case BOOL:
			return syntax.baseTypeSpecifier(lexer.next(Token.BOOL));
		default:
			throw new ParsingException();
		}
	}

	private Identifier parseIdentifier() {
		Lexeme ide = lexer.next(Token.IDE);

		return syntax.identifier(ide.getString());
	}

	private Block parseBlock() {
		lexer.next(Token.BRACE_OPEN);

		ArrayList<Command> commands = new ArrayList<>();
		while (lexer.lookahead() != Token.BRACE_CLOSE) {
			commands.add(parseCommand());
		}

		lexer.next(Token.BRACE_CLOSE);

		return syntax.block(commands);
	}

	private Command parseCommand() {
		switch (lexer.lookahead()) {
		case BRACE_OPEN:
			return parseBlock();
		case IF:
			return parseIf();
		case WHILE:
			return parseWhile();
		case RETURN:
			return parseReturn();
		case PRINT:
			return parsePrint();
		default:
			return parseAssignOrCall();
		}
	}

	private Command parseAssignOrCall() {
		Expression expression = parseExpression();

		if (lexer.nextIf(Token.SEMI) != null) {
			return syntax.procedure(expression);
		}

		return parseAssignRight(expression);
	}

	private Print parsePrint() {
		lexer.next(Token.PRINT);
		lexer.next(Token.PAREN_OPEN);

		Expression expression = parseExpression();

		lexer.next(Token.PAREN_CLOSE);
		lexer.next(Token.SEMI);

		return syntax.print(expression);
	}

	private Command parseAssign() {
		Expression lhs = parseExpression();

		return parseAssignRight(lhs);
	}

	private Command parseAssignRight(Expression lhs) {
		lexer.next(Token.ASSIGN);

		Expression rhs = parseExpression();

		lexer.next(Token.SEMI);

		return syntax.assign(lhs, rhs);
	}

	private Command parseReturn() {
		lexer.next(Token.RETURN);

		Expression expression = parseExpression();

		lexer.next(Token.SEMI);

		return syntax.returnCommand(expression);
	}

	private While parseWhile() {
		lexer.next(Token.WHILE);

		lexer.next(Token.PAREN_OPEN);

		Expression guard = parseExpression();

		lexer.next(Token.PAREN_CLOSE);

		Command command = parseCommand();

		return syntax.whileCommand(guard, command);
	}

	private Command parseIf() {
		lexer.next(Token.IF);
		lexer.next(Token.PAREN_OPEN);

		Expression guard = parseExpression();

		lexer.next(Token.PAREN_CLOSE);

		Command thenCommand = parseCommand();
		Command elseCommand = null;

		if (lexer.lookahead() == Token.ELSE) {
			lexer.next(Token.ELSE);
			elseCommand = parseCommand();
		}

		return syntax.ifCommand(guard, thenCommand, elseCommand);
	}

	private Expression parseExpression() {
		switch (lexer.lookahead()) {
		case NOT:
			return parseNotExpression();
		default:
			return parseOrExpression();
		}
	}

	private Expression parseOrExpression() {
		return parseOrExpressionRight(parseAndExpression());
	}

	private Expression parseOrExpressionRight(Expression expression) {
		if (lexer.nextIf(Token.OR) != null)
			return syntax.binary(BinaryOperator.OR, expression,
					parseAndExpression());

		return expression;
	}

	private Expression parseAndExpression() {
		return parseAndExpressionRight(parseCmpExpression());
	}

	private Expression parseAndExpressionRight(Expression expression) {
		if (lexer.nextIf(Token.AND) != null)
			return syntax.binary(BinaryOperator.AND, expression,
					parseCmpExpression());

		return expression;
	}

	private Expression parseCmpExpression() {
		return parseCmpExpressionRight(parseArithExpression());
	}

	private Expression parseCmpExpressionRight(Expression expression) {
		switch (lexer.lookahead()) {
		case EQ:
			lexer.next(Token.EQ);
			return syntax.binary(BinaryOperator.EQ, expression,
					parseArithExpression());
		case NEQ:
			lexer.next(Token.NEQ);
			return syntax.binary(BinaryOperator.NEQ, expression,
					parseArithExpression());
		default:
			return expression;
		}
	}

	private Expression parseArithExpression() {
		return parseArithExpressionRight(parseTimesExpression());
	}

	private Expression parseArithExpressionRight(Expression expression) {
		switch (lexer.lookahead()) {
		case PLUS:
			lexer.next(Token.PLUS);
			return syntax.binary(BinaryOperator.PLUS, expression,
					parseTimesExpression());
		case MINUS:
			lexer.next(Token.MINUS);
			return syntax.binary(BinaryOperator.PLUS, expression,
					parseTimesExpression());
		default:
			return expression;
		}
	}

	private Expression parseTimesExpression() {
		return parseTimesExpressionRight(parseProjExpression());
	}

	private Expression parseTimesExpressionRight(Expression expression) {
		if (lexer.nextIf(Token.TIMES) != null)
			return syntax.binary(BinaryOperator.TIMES, expression,
					parseProjExpression());

		return expression;
	}

	private Expression parseProjExpression() {
		return parseProjExpressionRight(parseChildExpression());
	}

	private Expression parseProjExpressionRight(Expression expression) {
		if (lexer.lookahead() == Token.PROJ_OR_TUPLE_CLOSE
				&& lexer.lookahead2() == Token.INT_LITERAL) {
			lexer.next(Token.PROJ_OR_TUPLE_CLOSE);
			return syntax.projection(expression, parseIntLiteral());
		}

		return expression;
	}

	private Expression parseChildExpression() {
		return parseChildExpressionRight(parseTreeExpression());
	}

	private Expression parseChildExpressionRight(Expression expression) {
		if (lexer.nextIf(Token.CHILD) != null)
			return syntax.binary(BinaryOperator.CHILD, expression,
					parseTreeExpression());

		return expression;
	}

	private Expression parseRootExpression() {
		return parseRootExpressionRight(parseAtomicExpression());
	}

	private Expression parseRootExpressionRight(Expression expression) {
		if (lexer.nextIf(Token.ROOT) != null)
			return syntax.unary(UnaryOperator.ROOT, expression);
		return expression;
	}

	private Expression parseTreeExpression() {
		return parseTreeExpressionRight(parseRootExpression());
	}

	private Expression parseTreeExpressionRight(Expression expression) {
		if (lexer.nextIf(Token.TREE_OPEN) != null) {
			ArrayList<Expression> children = new ArrayList<>();

			if (lexer.lookahead() != Token.TREE_CLOSE) {
				children.add(parseExpression());
			}

			while (lexer.lookahead() != Token.TREE_CLOSE) {
				lexer.next(Token.COMMA);
				children.add(parseExpression());
			}

			lexer.next(Token.TREE_CLOSE);

			return syntax.treeConstructor(expression, children);
		}

		return expression;
	}

	private Expression parseNotExpression() {
		lexer.next(Token.NOT);

		return syntax.unary(UnaryOperator.NOT, parseExpression());
	}

	private Expression parseAtomicExpression() {
		switch (lexer.lookahead()) {
		case PAREN_OPEN:
			return parseParenExpression();
		case IDE:
			return parseVariableOrCall();
		case INT_LITERAL:
			return parseIntLiteral();
		case DOUBLE_LITERAL:
			return parseDoubleLiteral();
		case STRING_LITERAL:
			return parseStringLiteral();
		case TRUE:
		case FALSE:
			return parseBoolLiteral();
		case NULL:
			return parseNullLiteral();
		case TUPLE_OPEN:
			return parseTupleConstructor();
		default:
			throw new ParsingException(String.format("Unexpected %s.",
					lexer.lookahead()));
		}
	}

	private Expression parseVariableOrCall() {
		Identifier identifier = parseIdentifier();

		if (lexer.lookahead() == Token.PAREN_OPEN) {
			return parseCallRight(identifier);
		}

		return syntax.variable(identifier);
	}

	private Call parseCallRight(Identifier identifier) {
		lexer.next(Token.PAREN_OPEN);

		ArrayList<Expression> arguments = new ArrayList<>();

		if (lexer.lookahead() != Token.PAREN_CLOSE) {
			arguments.add(parseExpression());
		}

		while (lexer.lookahead() != Token.PAREN_CLOSE) {
			lexer.next(Token.COMMA);
			arguments.add(parseExpression());
		}

		lexer.next(Token.PAREN_CLOSE);

		return syntax.call(identifier, arguments);
	}

	private TupleConstructor parseTupleConstructor() {
		lexer.next(Token.TUPLE_OPEN);

		ArrayList<Expression> items = new ArrayList<>();

		if (lexer.lookahead() != Token.PROJ_OR_TUPLE_CLOSE) {
			items.add(parseExpression());
		}

		while (lexer.lookahead() != Token.PROJ_OR_TUPLE_CLOSE) {
			lexer.next(Token.COMMA);
			items.add(parseExpression());
		}

		lexer.next(Token.PROJ_OR_TUPLE_CLOSE);

		return syntax.tupleConstructor(items);
	}

	private Expression parseBoolLiteral() {
		if (lexer.lookahead() == Token.TRUE)
			return syntax.literal(lexer.next(Token.TRUE));
		else
			return syntax.literal(lexer.next(Token.FALSE));
	}

	private Literal parseNullLiteral() {
		return syntax.literal(lexer.next(Token.NULL));
	}

	private Literal parseStringLiteral() {
		return syntax.literal(lexer.next(Token.STRING_LITERAL));
	}

	private Literal parseDoubleLiteral() {
		return syntax.literal(lexer.next(Token.DOUBLE_LITERAL));
	}

	private Literal parseIntLiteral() {
		return syntax.literal(lexer.next(Token.INT_LITERAL));
	}

	private VariableExpression parseVariable() {
		return syntax.variable(parseIdentifier());
	}

	private Expression parseParenExpression() {
		lexer.next(Token.PAREN_OPEN);
		Expression expression = parseExpression();
		lexer.next(Token.PAREN_CLOSE);

		return expression;
	}
}
