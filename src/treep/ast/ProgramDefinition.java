package treep.ast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import treep.gen.Emitter;
import treep.sem.Program;
import treep.sem.Semantics;
import treep.types.AtomicType;
import treep.types.Type;
import treep.types.TypeSystem;

public class ProgramDefinition {
	private List<FunctionDefinition> functions = new ArrayList<>();

	private Semantics semantics = new Semantics();
	private TypeSystem typeSystem = new TypeSystem();

	private Program program;

	public ProgramDefinition(List<FunctionDefinition> functions) {
		this.functions = functions;
	}

	public List<FunctionDefinition> getFunctionDefinitions() {
		return functions;
	}

	public void semanticAnalysis() {
		program = semantics.program();

		program.add(semantics.function("branchingfactor", AtomicType.INT,
				Collections.<Type> singletonList(typeSystem.nullType())));
		program.add(semantics.function("height", AtomicType.INT,
				Collections.<Type> singletonList(typeSystem.nullType())));
		program.add(semantics.function("empty", AtomicType.BOOL,
				Collections.<Type> singletonList(typeSystem.nullType())));
		program.add(semantics.function("type", AtomicType.STRING,
				Collections.<Type> singletonList(typeSystem.nullType())));

		for (FunctionDefinition function : functions) {
			function.prototypeAnalysis();
			program.add(function.getFunctionPrototype());
		}

		for (FunctionDefinition function : functions) {
			function.setProgram(program);
			function.semanticAnalysis();
		}
	}

	public void compile(Emitter emitter) {
		emitter.write("import java.util.ArrayList;\n");
		emitter.write("import treep.rts.*;\n\n");
		emitter.write("public class Main extends Library {\n\n");

		emitter.write("public static void main(String[] args) { main(); }\n\n");

		for (FunctionDefinition function : functions) {
			function.compile(emitter);
		}

		emitter.write("}");
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("PROGRAM:\n");

		for (FunctionDefinition function : functions) {
			sb.append(function.toString());
			sb.append("\n");
		}

		return sb.toString();
	}
}
