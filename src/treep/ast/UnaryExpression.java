package treep.ast;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;
import treep.sem.SemanticException;
import treep.types.AtomicType;
import treep.types.Type;
import treep.types.TypeSystem;

public class UnaryExpression extends Expression {
	public UnaryOperator op;
	public Expression operand;

	private TypeSystem typeSystem = new TypeSystem();
	private Type type;

	UnaryExpression(UnaryOperator op, Expression operand) {
		this.op = op;
		this.operand = operand;
	}

	public UnaryOperator getOperator() {
		return op;
	}

	public Expression getOperand() {
		return operand;
	}

	@Override
	public void semanticAnalysis() {
		operand.setScope(getScope());
		operand.setProgram(getProgram());

		operand.semanticAnalysis();

		type = inferType();
	}

	public Type inferType() {
		switch (op) {
		case NOT:
			return notType();
		case ROOT:
			return rootType();
		default:
			throw new AssertionError();
		}
	}

	private Type rootType() {
		return operand.getType();
	}

	private Type notType() {
		if (!typeSystem.assignableFrom(AtomicType.BOOL, operand.getType()))
			throw new SemanticException(String.format(
					"'!' operator requires bool argument. Found %s instead.",
					operand.getType()));
		return AtomicType.BOOL;
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public String toString() {
		return String.format("%s(%s)", op.toString(), operand.toString());
	}

	@Override
	public void onAllocateRegisters(RegisterAllocator registerAllocator) {
		operand.allocateRegisters(registerAllocator);
	}

	@Override
	public void compile(Emitter emitter) {
		operand.compile(emitter);
		emitter.write(String.format("Tree %s = %s.%s();\n", getOutRegister()
				.getName(), operand.getOutRegister().getName(), mapOp()));
	}

	private String mapOp() {
		switch (op) {
		case NOT:
			return "not";
		case ROOT:
			return "root";
		default:
			throw new AssertionError();
		}
	}

}
