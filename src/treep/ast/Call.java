package treep.ast;

import java.util.List;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;
import treep.sem.Function;
import treep.sem.SemanticException;
import treep.types.Type;
import treep.types.TypeSystem;

public class Call extends Expression {
	private Identifier identifier;
	private List<Expression> arguments;

	private Function function;

	private TypeSystem typeSystem = new TypeSystem();
	private Type type;

	Call(Identifier function, List<Expression> arguments) {
		this.identifier = function;
		this.arguments = arguments;
	}

	public Identifier getIdentifier() {
		return identifier;
	}

	public List<Expression> getArguments() {
		return arguments;
	}

	@Override
	public void semanticAnalysis() {
		function = getProgram().getFunction(identifier.getName());

		for (Expression expression : arguments) {
			expression.setScope(getScope());
			expression.setProgram(getProgram());
			expression.semanticAnalysis();
		}

		type = inferType();
	}

	private Type inferType() {
		if (arguments.size() != function.getArgumentTypes().size())
			throw new SemanticException(String.format(
					"Function %s expects %d arguments. %d given.",
					function.getName(), function.getArgumentTypes().size(),
					arguments.size()));

		for (int i = 0; i < function.getArgumentTypes().size(); i++)
			if (!typeSystem.assignableFrom(function.getArgumentTypes().get(i),
					arguments.get(i).getType()))
				throw new SemanticException(String.format(
						"Function %s expects %s as argument %d. %s given.",
						function.getName(), function.getArgumentTypes().get(i),
						i, arguments.get(i).getType()));

		return function.getReturnType();
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public void onAllocateRegisters(RegisterAllocator registerAllocator) {
		for (Expression argument : arguments)
			argument.allocateRegisters(registerAllocator);
	}

	@Override
	public void compile(Emitter emitter) {
		for (Expression argument : arguments)
			argument.compile(emitter);

		emitter.write(String.format("Tree %s = %s(",
				getOutRegister().getName(), function.getName()));

		boolean first = true;
		for (Expression argument : arguments) {
			if (!first)
				emitter.write(", ");
			emitter.write(String.format("%s.copy()", argument.getOutRegister()
					.getName()));
			first = false;
		}

		emitter.write(");\n");
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append(identifier.toString());

		sb.append("(");
		for (Expression e : arguments) {
			sb.append(e.toString());
			sb.append(",");
		}
		sb.append(")");

		return sb.toString();
	}
}
