package treep.ast;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;

public class While extends Command {

	private Expression guard;
	private Command command;

	public While(Expression guard, Command command) {
		this.guard = guard;
		this.command = command;
	}

	@Override
	public void semanticAnalysis() {
		guard.setProgram(getProgram());
		guard.setScope(getScope());

		command.setProgram(getProgram());
		command.setScope(getScope());

		guard.semanticAnalysis();
		command.semanticAnalysis();
	}

	@Override
	public void allocateRegisters(RegisterAllocator registerAllocator) {
		guard.allocateRegisters(registerAllocator);
		command.allocateRegisters(registerAllocator);
	}

	@Override
	public String toString() {
		return String.format("while (%s) %s", guard.toString(),
				command.toString());
	}

	@Override
	public void compile(Emitter emitter) {
		emitter.write("while(true) {\n");

		guard.compile(emitter);
		emitter.write(String.format("if (!%s.isTrue()) break;", guard
				.getOutRegister().getName()));

		command.compile(emitter);

		emitter.write("}\n");
	}
}
