package treep.ast;

public enum BinaryOperator {
	AND,
	OR,
	EQ,
	NEQ,
	PLUS,
	MINUS,
	TIMES,
	CHILD
}
