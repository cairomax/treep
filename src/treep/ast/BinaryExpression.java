package treep.ast;

import treep.gen.Emitter;
import treep.gen.Register;
import treep.gen.RegisterAllocator;
import treep.sem.SemanticException;
import treep.types.AtomicType;
import treep.types.Type;
import treep.types.TypeSystem;

public class BinaryExpression extends Expression {
	private BinaryOperator op;

	private Expression left;
	private Expression right;

	private Type type;

	private TypeSystem typeSystem = new TypeSystem();

	private Register outRegister;

	BinaryExpression(BinaryOperator op, Expression left, Expression right) {
		this.op = op;
		this.left = left;
		this.right = right;
	}

	public BinaryOperator getOperator() {
		return op;
	}

	public Expression getLeftOperand() {
		return left;
	}

	public Expression getRight() {
		return right;
	}

	@Override
	public void semanticAnalysis() {
		left.setProgram(getProgram());
		right.setProgram(getProgram());

		left.setScope(getScope());
		right.setScope(getScope());

		left.semanticAnalysis();
		right.semanticAnalysis();

		type = inferType();
	}

	@Override
	public Type getType() {
		return type;
	}

	public Type inferType() {
		switch (op) {
		case AND:
		case OR:
			return booleanType();
		case PLUS:
		case MINUS:
		case TIMES:
			return arithType();
		case CHILD:
			return childType();
		case EQ:
		case NEQ:
			return cmpType();
		default:
			throw new AssertionError();
		}
	}

	private Type cmpType() {
		// make sure they are compatible
		typeSystem.commonType(left.getType(), right.getType());

		return AtomicType.BOOL;
	}

	private Type childType() {
		if (!typeSystem.assignableFrom(AtomicType.INT, right.getType()))
			throw new SemanticException(
					String.format(
							"'@' operator requires int as second argument. Found %s instead.",
							right.getType()));

		return left.getType();
	}

	private Type arithType() {
		Type common = typeSystem.commonType(left.getType(), right.getType());

		if (!typeSystem.assignableFrom(AtomicType.INT, common)
				&& !typeSystem.assignableFrom(AtomicType.DOUBLE, common))
			throw new SemanticException(
					String.format(
							"Boolean operations require numeric arguments. Found %s and %s instead.",
							left.getType(), right.getType()));

		return common;
	}

	private Type booleanType() {
		Type common = typeSystem.commonType(left.getType(), right.getType());

		if (!typeSystem.assignableFrom(AtomicType.BOOL, common))
			throw new SemanticException(
					String.format(
							"Boolean operations require bool arguments. Found %s and %s instead.",
							left.getType(), right.getType()));

		return common;
	}

	@Override
	public void onAllocateRegisters(RegisterAllocator registerAllocator) {
		left.allocateRegisters(registerAllocator);
		right.allocateRegisters(registerAllocator);
	}

	@Override
	public void compile(Emitter emitter) {
		left.compile(emitter);
		right.compile(emitter);

		emitter.write(String.format("Tree %s = Tree.%s(%s, %s);\n",
				getOutRegister().getName(), op.name().toLowerCase(), left
						.getOutRegister().getName(), right.getOutRegister()
						.getName()));
	}

	@Override
	public String toString() {
		return String.format("(%s %s %s)", left, op, right);
	}
}
