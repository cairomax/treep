package treep.ast;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;
import treep.sem.SemanticException;
import treep.sem.Semantics;
import treep.sem.Variable;
import treep.types.TypeSystem;

public class Assignment extends Command {
	private Expression lhs;
	private Expression rhs;

	private Semantics semantics = new Semantics();
	private TypeSystem typeSystem = new TypeSystem();

	private Variable lvalue;

	Assignment(Expression lhs, Expression rhs) {
		this.lhs = lhs;
		this.rhs = rhs;
	}

	public Expression getLhs() {
		return lhs;
	}

	public Expression getRhs() {
		return rhs;
	}

	@Override
	public void semanticAnalysis() {
		rhs.setScope(getScope());
		rhs.setProgram(getProgram());

		rhs.semanticAnalysis();

		handleImplicitDefinition();

		lhs.setProgram(getProgram());
		lhs.setScope(getScope());

		lhs.semanticAnalysis();

		if (!typeSystem.assignableFrom(lhs.getType(), rhs.getType()))
			throw new SemanticException(String.format(
					"Incompatible types in assigment: %s <- %s.",
					lhs.getType(), rhs.getType()));
	}

	private void handleImplicitDefinition() {
		if (!(lhs instanceof VariableExpression))
			return;

		VariableExpression variableExpression = (VariableExpression) lhs;

		Variable variable = getScope().lookup(variableExpression.getName());

		if (variable == null) {
			lvalue = semantics.variable(variableExpression.getName(),
					rhs.getType());
			getScope().add(lvalue);
		}
	}

	@Override
	public void allocateRegisters(RegisterAllocator registerAllocator) {
		lhs.allocateRegisters(registerAllocator);
		rhs.allocateRegisters(registerAllocator);
	}

	@Override
	public String toString() {
		return String.format("%s = %s", lhs, rhs);
	}

	@Override
	public void compile(Emitter emitter) {
		if (lvalue != null)
			emitter.write(String.format("Tree %s = Tree.create();\n", lvalue
					.getRegister().getName()));

		lhs.compile(emitter);
		rhs.compile(emitter);

		emitter.write(String.format("%s.assign(%s);\n", lhs.getOutRegister()
				.getName(), rhs.getOutRegister().getName()));
	}
}
