package treep.ast;

import treep.gen.Emitter;
import treep.gen.Register;
import treep.gen.RegisterAllocator;
import treep.sem.Program;
import treep.sem.Scope;
import treep.types.Type;

public abstract class Expression {
	private Scope scope;
	private Program program;
	private Register outRegister;

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public void setProgram(Program program) {
		this.program = program;
	}

	public Scope getScope() {
		return scope;
	}

	public Program getProgram() {
		return program;
	}

	public abstract Type getType();

	public abstract void semanticAnalysis();

	public abstract void onAllocateRegisters(RegisterAllocator registerAllocator);

	public void allocateRegisters(RegisterAllocator registerAllocator) {
		outRegister = registerAllocator.nextExpr();
		onAllocateRegisters(registerAllocator);
	}

	public Register getOutRegister() {
		return outRegister;
	}

	public abstract void compile(Emitter emitter);
}
