package treep.ast;

import java.util.ArrayList;
import java.util.List;

import treep.lex.Lexeme;

public class Syntax {
	public BinaryExpression binary(BinaryOperator op, Expression left,
			Expression right) {
		return new BinaryExpression(op, left, right);
	}

	public UnaryExpression unary(UnaryOperator op, Expression operand) {
		return new UnaryExpression(op, operand);
	}

	public Identifier identifier(String name) {
		return new Identifier(name);
	}

	public FunctionDefinition functionDefinition(
			TypeSpecifier returnTypeSpecifier, Identifier identifier,
			List<Argument> arguments, Block block) {
		return new FunctionDefinition(returnTypeSpecifier, identifier,
				arguments, block);
	}

	public ProgramDefinition program(List<FunctionDefinition> functions) {
		return new ProgramDefinition(functions);
	}

	public Block block(ArrayList<Command> commands) {
		return new Block(commands);
	}

	public Assignment assign(Expression lhs, Expression rhs) {
		return new Assignment(lhs, rhs);
	}

	public If ifCommand(Expression guard, Command thenCommand,
			Command elseCommand) {
		return new If(guard, thenCommand, elseCommand);
	}

	public VariableExpression variable(Identifier identifier) {
		return new VariableExpression(identifier);
	}

	public Return returnCommand(Expression expression) {
		return new Return(expression);
	}

	public Literal literal(Lexeme lexeme) {
		return new Literal(lexeme);
	}

	public TreeConstructor treeConstructor(Expression root,
			ArrayList<Expression> children) {
		return new TreeConstructor(root, children);
	}

	public TupleConstructor tupleConstructor(ArrayList<Expression> items) {
		return new TupleConstructor(items);
	}

	public Call call(Identifier function, ArrayList<Expression> arguments) {
		return new Call(function, arguments);
	}

	public Print print(Expression expression) {
		return new Print(expression);
	}

	public While whileCommand(Expression guard, Command command) {
		return new While(guard, command);
	}

	public ProcedureCall procedure(Expression expression) {
		return new ProcedureCall(expression);
	}

	public BaseTypeSpecifier baseTypeSpecifier(Lexeme lexeme) {
		return new BaseTypeSpecifier(lexeme);
	}

	public TupleTypeSpecifier tupleTypeSpecifier(
			ArrayList<TypeSpecifier> itemTypes) {
		return new TupleTypeSpecifier(itemTypes);
	}

	public Argument argument(TypeSpecifier typeSpecifier, Identifier identifier) {
		return new Argument(typeSpecifier, identifier);
	}

	public Projection projection(Expression expression, Literal intLiteral) {
		return new Projection(expression, intLiteral);
	}
}
