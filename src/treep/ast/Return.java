package treep.ast;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;

public class Return extends Command {

	private Expression expression;

	public Return(Expression expression) {
		this.expression = expression;
	}

	public Expression getExpression() {
		return expression;
	}

	@Override
	public void semanticAnalysis() {
		expression.setScope(getScope());
		expression.setProgram(getProgram());

		expression.semanticAnalysis();
	}

	@Override
	public String toString() {
		return String.format("return %s ;", expression.toString());
	}

	@Override
	public void allocateRegisters(RegisterAllocator registerAllocator) {
		expression.allocateRegisters(registerAllocator);
	}

	@Override
	public void compile(Emitter emitter) {
		expression.compile(emitter);
		// if(true) disables dead code detection
		emitter.write(String.format("if(true) return %s;\n", expression
				.getOutRegister().getName()));
	}
}
