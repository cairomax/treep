package treep.ast;

import treep.gen.Emitter;
import treep.sem.Semantics;
import treep.sem.Variable;

public class Argument {

	private TypeSpecifier typeSpecifier;
	private Identifier identifier;

	private Semantics semantics = new Semantics();
	private Variable variable;

	Argument(TypeSpecifier typeSpecifier, Identifier identifier) {
		this.typeSpecifier = typeSpecifier;
		this.identifier = identifier;
	}

	@Override
	public String toString() {
		return String.format("%s %s", typeSpecifier, identifier);
	}

	public void semanticAnalysis() {
		variable = semantics.variable(identifier.getName(),
				typeSpecifier.getType());
	}

	public Variable getVariable() {
		return variable;
	}

	public void emit(Emitter emitter) {
		emitter.write("Tree " + getVariable().getRegister().getName());
	}

}
