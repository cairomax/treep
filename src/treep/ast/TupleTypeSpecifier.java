package treep.ast;

import java.util.ArrayList;
import java.util.List;

import treep.types.TupleType;
import treep.types.Type;
import treep.types.TypeSystem;

public class TupleTypeSpecifier extends TypeSpecifier {

	private List<TypeSpecifier> itemTypeSpecifiers;
	private TypeSystem typeSystem = new TypeSystem();

	TupleTypeSpecifier(ArrayList<TypeSpecifier> itemTypes) {
		this.itemTypeSpecifiers = itemTypes;
	}

	public List<TypeSpecifier> getItemTypeSpecifiers() {
		return itemTypeSpecifiers;
	}

	@Override
	public TupleType getType() {
		ArrayList<Type> itemTypes = new ArrayList<>();
		for (TypeSpecifier typeSpecifier : itemTypeSpecifiers) {
			itemTypes.add(typeSpecifier.getType());
		}

		return typeSystem.tupleType(itemTypes);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
	
		sb.append("TT(");
	
		for (TypeSpecifier type : itemTypeSpecifiers) {
			sb.append(type.toString());
			sb.append(", ");
		}
	
		sb.append(")");
	
		return sb.toString();
	}

}
