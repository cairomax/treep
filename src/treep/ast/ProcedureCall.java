package treep.ast;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;

public class ProcedureCall extends Command {

	private Expression expression;

	public ProcedureCall(Expression expression) {
		this.expression = expression;
	}

	@Override
	public void semanticAnalysis() {
		expression.setScope(getScope());
		expression.setProgram(getProgram());

		expression.semanticAnalysis();
	}

	@Override
	public String toString() {
		return String.format("PROC: %s ;", expression.toString());
	}

	@Override
	public void allocateRegisters(RegisterAllocator registerAllocator) {
		expression.allocateRegisters(registerAllocator);
	}

	@Override
	public void compile(Emitter emitter) {
		expression.compile(emitter);
	}
}
