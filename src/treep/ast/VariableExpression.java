package treep.ast;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;
import treep.sem.Variable;
import treep.types.Type;

public class VariableExpression extends Expression {

	private Identifier identifier;

	private Variable variable;

	public VariableExpression(Identifier identifier) {
		this.identifier = identifier;
	}

	@Override
	public String toString() {
		return String.format("Var(%s)", identifier.toString());
	}

	public String getName() {
		return identifier.getName();
	}

	@Override
	public void semanticAnalysis() {
		variable = getScope().get(getName());
	}

	@Override
	public Type getType() {
		return variable.getType();
	}

	@Override
	public void onAllocateRegisters(RegisterAllocator registerAllocator) {
	}

	@Override
	public void compile(Emitter emitter) {
		emitter.write(String.format("Tree %s = %s;\n", getOutRegister()
				.getName(), variable.getRegister().getName()));
	}
}
