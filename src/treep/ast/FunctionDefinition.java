package treep.ast;

import java.util.ArrayList;
import java.util.List;

import treep.gen.Emitter;
import treep.gen.RegisterAllocator;
import treep.sem.Function;
import treep.sem.Program;
import treep.sem.Scope;
import treep.sem.Semantics;
import treep.types.Type;

public class FunctionDefinition {
	private TypeSpecifier returnTypeSpecifier;
	private Identifier identifier;
	private List<Argument> arguments;
	private Block block;

	private Semantics semantics = new Semantics();

	private Program program;
	private Function function;
	private Scope argumentsScope;
	private RegisterAllocator registerAllocator;

	public FunctionDefinition(TypeSpecifier returnTypeSpecifier,
			Identifier identifier, List<Argument> arguments, Block block) {
		this.returnTypeSpecifier = returnTypeSpecifier;
		this.identifier = identifier;
		this.arguments = arguments;
		this.block = block;
	}

	public void prototypeAnalysis() {
		for (Argument argument : arguments) {
			argument.semanticAnalysis();
		}

		ArrayList<Type> argumentTypes = new ArrayList<>();

		for (Argument argument : arguments) {
			argumentTypes.add(argument.getVariable().getType());
		}

		function = semantics.function(identifier.getName(),
				returnTypeSpecifier.getType(), argumentTypes);
	}

	public void semanticAnalysis() {
		argumentsScope = semantics.scope(null);

		for (Argument argument : arguments) {
			argumentsScope.add(argument.getVariable());
		}

		block.setScope(argumentsScope);
		block.setProgram(program);

		block.semanticAnalysis();
	}

	public Function getFunctionPrototype() {
		return function;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(identifier.toString());

		sb.append("(");
		for (Argument argument : arguments) {
			sb.append(argument.toString());
			sb.append(",");
		}
		sb.append(")");

		sb.append(block.toString());

		return sb.toString();
	}

	public void setProgram(Program program) {
		this.program = program;
	}

	public void compile(Emitter emitter) {
		registerAllocator = new RegisterAllocator();

		for (Argument argument : arguments) {
			argument.getVariable().allocateRegisters(registerAllocator);
		}

		block.allocateRegisters(registerAllocator);

		emitter.write(String.format("public static Tree %s (\n",
				function.getName()));

		boolean first = true;
		for (Argument argument : arguments) {
			if (!first)
				emitter.write(", ");
			argument.emit(emitter);
			first = false;
		}

		emitter.write(") {");

		block.compile(emitter);
		emitter.write("return null;\n");

		emitter.write("}\n");
	}
}
