int depthFirstSearch(<string,int> tree){
    if(tree == null)
        return null;
         
    print(tree #> 0);
     
    tree #> 1 = 1;
    
    child = 0; 
    while(tree @ child != null) {
        if((tree @ child)#> 1 == 0) {
            depthFirstSearch(tree @ child);
        }
        child = child + 1; 
    }
}

int test1(int tree1, int tree2){
    return 0[tree1, tree2];
}

int test2(){
    print("DFS test - expected: 1, 2, 3, 4, 5");
    return depthFirstSearch(<"1", 0>[<"2", 0>[<"3", 0>], <"4", 0>[<"5", 0>]]);
}

int test3(){
    print("branching factor - expected: 0, 1, 3");
    print(branchingfactor(0));
    print(branchingfactor(0[1]));
    print(branchingfactor(0[1[1,2,3],2]));
}

int test4(){
    print("height - expected: 0, 1, 2, 4");
    print(height(null));
    print(height(0));
    print(height(0[1]));
    print(height(0[1[1,2,3[1]],2]));
}

int test5(){
    print("types - expected: null, int, string, double, bool");
    print(type(null));
    print(type(0));
    print(type("a"));
    print(type(0.0));
    print(type(true));
}

int test6(){
    print("empty - expected: true, false, false");
    print(empty(null));
    print(empty(0));
    print(empty("a"[null]));
}

int main(){
    test1(1, 2);
    test2();
    test3();
    test4();
    test5();
    test6();
}